# Flask Sample API

## はじめに
Flaskを用いたWEB APIのサンプルコードです。
本コードでは、app.pyに直接書き出したdictの中身をjsonに変換したものをresponseとしています。

## End Points
* /livehouses
* /players

## Local Start
「$ python3 app.py」