# -*- coding: utf-8 -*-
from flask import Flask, jsonify, request, render_template
app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False # <= 日本語の文字化け対策

# ライブハウスの情報
livehouses_json = [{"name": "Livehouse Output", "address": "沖縄県那覇市久米2-5-7 久米ビルB1"},
                        {"name": "LIVE HOUSE MOD'S", "address": "沖縄県中頭郡北谷町美浜9番地1　デポアイランドビルE館2F"},
                        {"name": "ミュージックタウン音市場", "address": "沖縄県沖縄市上地1-1-1 3F"},
                        {"name": "もーあしび チャクラ", "address": "沖縄県那覇市牧志１丁目２−１"}]

# 演者  の情報
players_json = [{"name": "スーさん", "instrument": "ギター"}, {"name": "おんちゃん😍", "instrument":"ボーカル"}]

@app.route("/", methods=["GET"])
def index():
    return render_template("index.html")

# 以下API群
@app.route('/livehouses', methods=["GET"])
def livehouses():
    return jsonify(livehouses_json)

@app.route('/players', methods=["GET"])
def players():
    return jsonify(players_json)

if __name__ == "__main__":
    app.debug = True
    # app.run(host='0.0.0.0', port=80) # <= Production
    app.run() # <= local debug test
